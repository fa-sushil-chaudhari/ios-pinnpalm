# Pod::Spec.new do |s|
#   s.name = "Pinn"
#   # s.version = %x(git describe --tags --abbrev=0).chomp
#   s.version = "0.0.2"
#   s.summary = "Pinn SDK"
#   s.homepage = "https://github.com/pinntech/ios-sdk"
#   # s.license = { type: "", file: "" }
#   s.author = {
#     "Pinn Technologies" => "support@pinn.ai",
#   }
#   s.social_media_url = ""
#   s.platform = :ios, "10.0"
#   # s.source = { git: "https://github.com/pinntech/ios-sdk.git", tag: "#{s.version}" }
#   s.source = { :path => '.' }
#   s.source_files = "Sources/#{s.name}/**/*.{swift,h,m}"
#   # s.module_map = "Sources/#{s.name}/module.modulemap"
#   s.public_header_files = "Sources/#{s.name}.h"
#   s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '$(PODS_ROOT)/Pinn/' }
#   s.frameworks = 'PinnPalm'
# end


# Pod::Spec.new do |s|
#   s.name = "pinn-sdk"
#   #s.version = %x(git describe --tags --abbrev=0).chomp
#   s.version = "1.5.0"
#   s.summary = "Pinn SDK"
#   s.homepage = "https://bitbucket.org/tlainnovation/ios-sdk"
#   #s.license = { type: "", file: "" }
#   s.license = "MIT"
#   s.author = { "TLA Innovations" => "support@tlainnovation.io" }
#   #s.social_media_url = ""
#   #s.platform = :ios, "10.0"
#   # s.source       = { :path => '.' }
#   # s.source = { :path => '../ios-sdk' }
#   s.source = { git: "git@bitbucket.org:tlainnovation/ios-sdk.git",  :tag => s.version }
#   #   tag: "v#{s.version}"
#   s.source_files = "Sources/#{s.name}/**/*.{swift,h,m}"
#   s.exclude_files = "Classes/Exclude"
#   s.module_map = "Sources/#{s.name}/module.modulemap"
#   # s.public_header_files = "Sources/#{s.name}/#{s.name}.h"
#   s.public_header_files = "Sources/#{s.name}.h"
#   # s.framework        = 'PinnPalm'
#   s.ios.deployment_target = "12.0"
#   s.vendored_frameworks = 'Pinn.framework'
#   # s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '$(PODS_ROOT)/Pinn/' }
#   # s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => "." }
#   s.frameworks          =  "AVFoundation","Foundation","UIKit","Accelerate"
#   s.requires_arc = true
  
#   s.dependency "ios-pinnpalm","~> 0.1.0"
# #  s.subspec 'ios-pinnpalm' do |ss|
# #    ss.source = { git: 'https://fa-sushil-chaudhari@bitbucket.org/fa-sushil-chaudhari/ios-pinnpalm.git' }
# #  end
# #  s.static_framework = true
#  s.pod_target_xcconfig = {
#    'VALID_ARCHS' =>  'armv7 armv7s x86_64 arm64',
#  }
  
#   # s.static_framework = true
#   #  s.vendored_frameworks = 'PinnPalm.framework'
#   # s.frameworks = 'PinnPalm'
#   # s.subspec "PinnPalm" do |ss|
#   #   ss.source_files = 'PinnPalm/**/*.h'
#   # end
#   # s.ios.vendored_frameworks = 'Pinn.framework'
#   # s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '/Applications/Xcode.app/Contents/Developer/Library/Frameworks' }
# end
# # https://www.dropbox.com/s/9pru3j5zs53zq2u/Pinn.zip?dl=0





Pod::Spec.new do |s|
  s.name = "pinn-sdk"
  #s.version = %x(git describe --tags --abbrev=0).chomp
  s.version = "1.4.0"
  s.summary = "Pinn SDK"
  s.homepage = "https://bitbucket.org/tlainnovation/ios-sdk"
  #s.license = { type: "", file: "" }
  s.license = "MIT"
  s.author = { "TLA Innovations" => "support@tlainnovation.io" }
  #s.social_media_url = ""
  #s.platform = :ios, "10.0"
  # s.source       = { :path => '.' }
  # s.source = { :path => '../ios-sdk' }
   s.source       = { :http => "https://www.dropbox.com/s/9pru3j5zs53zq2u/Pinn.zip?dl=1" }
  # s.source = { git: "git@bitbucket.org:tlainnovation/ios-sdk.git",  :tag => "v#{s.version}"}
  #   tag: "v#{s.version}"
  # s.source_files = "Sources/#{s.name}/**/*.{swift,h,m}"
  # s.exclude_files = "Classes/Exclude"
  #  s.module_map = "Sources/#{s.name}/module.modulemap"
  # s.public_header_files = "Sources/#{s.name}/#{s.name}.h"
  # s.public_header_files = "Sources/#{s.name}.h"
  # s.framework        = 'PinnPalm'
  s.ios.deployment_target = "12.0"
  # s.vendored_frameworks = 'PinnPalm.framework'
  # s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '$(PODS_ROOT)/Pinn/' }
  # s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => "." }
  # s.frameworks          =  "AVFoundation","Foundation","UIKit","Accelerate"
  # s.requires_arc = true
  
   s.dependency "ios-pinnpalm","~> 0.1.0"
    s.vendored_frameworks = "Pinn.framework"
#  s.subspec 'ios-pinnpalm' do |ss|
#    ss.source = { git: 'https://fa-sushil-chaudhari@bitbucket.org/fa-sushil-chaudhari/ios-pinnpalm.git' }
#  end
#  s.static_framework = true
#  s.pod_target_xcconfig = {
#    'VALID_ARCHS' =>  'armv7 armv7s x86_64 arm64',
#  }
  
  # s.static_framework = true
  #  s.vendored_frameworks = 'PinnPalm.framework'
  # s.frameworks = 'PinnPalm'
  # s.subspec "PinnPalm" do |ss|
  #   ss.source_files = 'PinnPalm/**/*.h'
  # end
  # s.ios.vendored_frameworks = 'Pinn.framework'
  # s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '/Applications/Xcode.app/Contents/Developer/Library/Frameworks' }
  s.pod_target_xcconfig = {
      'VALID_ARCHS' =>  'armv7 armv7s x86_64 arm64',
   }
end